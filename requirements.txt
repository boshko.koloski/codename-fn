tqdm==4.47.0
numpy==1.18.5
nltk==3.5
gensim==3.8.3
scikit_learn==0.23.2
