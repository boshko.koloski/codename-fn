#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 13:03:17 2020

@author: daskalot
"""

import kg_embedders 
import pickle
import pandas as pd
import numpy as np
class Term2Vec():
    def __init__(self, texts, name = "name"):
        self.name = name
        self.texts = texts
        self.embeddings = None
        
    def fit(self, top_n = 20, kg_model = "transe", normalization = "equal"):
        self.embeddings = kg_embedders.embedd(self.texts, top_n, kg_model, normalization, self.name)
        return self

    def doc2feature(self, doc):
        return self.embeddings[doc]
    
    def _export(self,path="term2vec.pkl"):
        self._model.save(path)

    def _import(self,path="term2vec.pkl"):
        self._model = pickle.load(open(path,'rb')) 
        
    def to_matrix(self):
        return np.array(self.embeddings)
    
    def _exportModel(self,name="term2vec", export_type = "csv"):
        if export_type == "csv":
            df = pd.DataFrame(self.to_matrix())
            df.to_csv(name+".csv", header = None, index = False)
        if export_type == "pkl":
            pickle.dump(self.to_matrix(), open(name+".pkl", "wb"))
        
    def _importModel(self,name="term2vec", import_type = "csv"):
        if import_type == "csv":
            df = pd.read_csv(name+".csv", header = None)
            self.embeddings = df.values
        if import_type == "pkl":
            self.embeddings = pickle.load(open(name+".pkl",'rb'))
        return self.embeddings