# Term2Vec : Knowledge graph-based document embedding enrichment

## Repository of diploma thesis  

### Train the KG embeddings

1. Download and install GraphVite library 
    -    Follow the link https://graphvite.io/docs/0.1.0/install.html \
     !!! Note the library needs CUDA based GPU !!! \
     !!! PREFERABLY RUN ON UNIX BASED OS !!!
2. Navigate to the [KG folder](./src/kg)  
3. Run graphvite *Name of the file*
    - Example: \
    ` graphvite run transe_wikidata5m.yaml `

### GermEval task

1. Find authors present in the KG by running the \
    - `python3 ./src/germeval/a2k.py`
    - This will generate a pickled dictionary of kg_embedding -> author -> THE EMBEDDING, where kg_embedding is any of the ( TransE | RotatE | SimplE | QuatE | DistMult | ComplEx) 
    - now the KG embeddings of authors are prepared. 
2. Run any of the vectorizaitons 
    - BERT: `python3 ./src/germeval/bertModel.py`
    - D2V: `python3 ./src/germeval/D2V.py`
    - LSA: `python3 ./src/germeval/lsa.py`
3. Each vectorization produces final .TSV file with statistics of the runnings and the executions, samples of such executions are in the `./expr`

### FakeNews task

1. Run ``` python3 ./src/fakenews/prep.py ```
2. Steps taken by prep:
    - This will extract kg_embeddings for each word present in each tweets collection and in the KB
    - Generate different LSA embeedings (n=features,d=dimensions) and train 2 classifiers 
    - Evaluate the classifiers
    - Export best results and models







