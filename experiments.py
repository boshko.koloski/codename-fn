#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 12:47:09 2020

@author: Bosec
"""

import vectorization
import embedding_binder 
import pandas as pd
import csv

def experiments_tm_datasets_test_val():
    dataset_path = { "drugs" : "drugs-rating",
                     "covid_fake" : "AAAI2021_COVID19_fake_news",
                     "fake_news_spreaders" : "pan-2020-fake-news",
                     "counterfactuals": "counterfactuals",
                     "hatespeech" : "hatespeech"}
    datasets = {}
    for dataset in dataset_path:
        data_dict = {"train" : None, "dev" : None, "test" : None}
        for mode in data_dict:
            path = "data/"+dataset+"/"+mode+".tsv"
            data_dict[mode] = pd.read_csv(path, sep="\t", quoting = csv.QUOTE_NONE, escapechar = ' ')
        datasets[dataset] = data_dict
    
def experiments_tm_datasets_cv(dataset_path, to_return = False):
    #datasets = {}
    for dataset in dataset_path:
        states = ["train", "dev", "test"]
        cv_data = {"text_a" : [], "label" : []}
        for state in states:
            path = "data/"+dataset_path[dataset]+"/"+state+".tsv"
            temp = pd.read_csv(path, sep="\t", quoting = csv.QUOTE_NONE, escapechar = ' ')
            cv_data['text_a'] = cv_data['text_a'] + temp['text_a'].tolist()
            cv_data['label'] = cv_data['label'] + temp['label'].tolist()
        if to_return:
            yield (dataset, dataset_path[dataset], cv_data)


        
def execute(dataset_path, kg_models, bert_models, hyper_params):
    for dataset, path, cv_data in experiments_tm_datasets_cv(dataset_path, True):
        experiment_data = {dataset : cv_data}
        labels = cv_data['label']
        assert len(cv_data['label']) == len(cv_data['text_a'])
        vectorization.vectorize(experiment_data, bert_models, kg_models)
        embedding_binder.train_env(dataset, labels, visualize=True, export=True, hyper_params = hyper_params[dataset])


def test_env():
    bert_models = ["distilbert-base-nli-mean-tokens"]
    kg_models = ["transe", "distmult", "complex"]
    hyper_params = { "covid_fake" : {'params' : {'batch_size': 60, 'shuffle': True}, 'max_epochs' : 100},
                     "counterfactuals": {'params' : {'batch_size': 60, 'shuffle': True}, 'max_epochs' : 100},
                     "hatespeech" : {'params' : {'batch_size': 60, 'shuffle': True}, 'max_epochs' : 100}
                  }
    dataset_path = { "covid_fake" : "AAAI2021_COVID19_fake_news",
                     "counterfactuals": "counterfactuals",
                     "hatespeech" : "hatespeech"}
    execute(dataset_path, kg_models, bert_models, hyper_params)

def test_env_single():
    bert_models = ["distilbert-base-nli-mean-tokens"]
    kg_models = ["transe", "distmult", "complex"]
    dataset_path = {"fake_news_spreaders" : "pan-2020-fake-news"}
    hyper_params = { "fake_news_spreaders" : {'params' : {'batch_size': 30, 'shuffle': True}, 'max_epochs' : 500}}
    execute(dataset_path, kg_models, bert_models, hyper_params)
    
test_env()