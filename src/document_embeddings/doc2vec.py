#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 14:27:09 2020

@author: bosko
"""

import numpy as np
import pickle
import pandas as pd
import multiprocessing
from tqdm import tqdm
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.base import BaseEstimator
from sklearn import utils as skl_utils

#Doc2Vec
class Doc2VecTransformer(BaseEstimator):
    def __init__(self,docs, vector_size=100, learning_rate=0.02, epochs=20):
        
        self.docs = docs
        self.learning_rate = learning_rate
        self.epochs = epochs
        self._model = None
        self.X = None
        self.vector_size = vector_size
        self.workers = multiprocessing.cpu_count() - 1
        
    def fit(self):
        tagged_x = []
        for doc in self.docs:
            temp_doc = TaggedDocument(self.docs[doc].split(),[doc])
            tagged_x.append(temp_doc)               
        model = Doc2Vec(documents=tagged_x, vector_size=self.vector_size, workers=self.workers)
        for epoch in range(self.epochs):
            model.train(skl_utils.shuffle([x for x in tqdm(tagged_x)]), total_examples=len(tagged_x), epochs=1)
            model.alpha -= self.learning_rate
            model.min_alpha = model.alpha
        self._model = model
        self.X = self.transform()
        return self


    def transform(self):
       return np.asmatrix(np.array([self._model.infer_vector(self.docs[doc].split()) for doc in self.docs]))
    
    def doc2feature(self,doc):
       return np.array(self._model.infer_vector(self.docs[doc].split()))
    
    def to_matrix(self):
        return self.X 
 
    def _export(self,path="d2vmodel.pkl"):
        pickle.dump(self._model, open(path, "wb"))
        
    def _import(self,path="d2vmodel.pkl"):
        self._model = pickle.load(open(path,'rb'))

    def _exportModel(self,name="d2v_sentences", export_type = "csv"):
        if export_type == "csv":
            df = pd.DataFrame(self.X)
            df.to_csv(name+".csv", header = None, index = False)
        if export_type == "pkl":
            pickle.dump(self.to_matrix(), open(name+".pkl", "wb"))
        
    def _importModel(self,name="d2v_sentences", import_type = "csv"):
        if import_type == "csv":
            df = pd.read_csv(name+".csv", header = None)
            self.X = df.values
        if import_type == "pkl":
            self.X = pickle.load(open(name+".pkl",'rb'))
        return self.X
    """
    def _importModel(self,path="d2v_sentences.pkl"):
        self.X = pickle.load(open(path,'rb'))
        return self.X
    
    def _exportModel(self,path="d2vmodel_sentences.pkl"):
        pickle.dump(self.X, open(path, "wb"))
    """
